from bs4 import BeautifulSoup
from selenium import webdriver
import time
import re
import json
import os
import pandas as pd
import multiprocessing
from Scraper.get_list_product import get_website_category, get_list_product
from Scraper.structure.home_page import HomePage
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities


def main():
    # use chrome drive
    chrome_options = webdriver.ChromeOptions()
    prefs = {'profile.managed_default_content_settings.images': 2}
    chrome_options.add_experimental_option("prefs", prefs)
    driver = webdriver.Chrome(executable_path='/home/tungnk/chromedriver_linux64/chromedriver', chrome_options=chrome_options)
    n_product = 0
    # use phantomjs drive
    # dcap = dict(DesiredCapabilities.PHANTOMJS)
    # dcap["phantomjs.page.settings.userAgent"] = (
    #     "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/53 "
    #     "(KHTML, like Gecko) Chrome/15.0.87"
    # )
    # driver = webdriver.PhantomJS(desired_capabilities=dcap, executable_path='/home/tungnk/Downloads/phantomjs-2.1.1-linux-x86_64/bin/phantomjs')

    page = HomePage(
        name='shopee',
        url='https://shopee.vn',
        cat_tag='_18Mo48',
        sub1_cat_tag='_1Nw60R'
    )

    if os.path.exists('data/{}/category.json'.format(page.name)):
        with open('data/{}/category.json'.format(page.name), 'r', encoding='utf-8') as pf:
            cate_dict = json.load(pf)
            page.name = cate_dict['name']
            page.url = cate_dict['url']
    else:
        cate_dict = get_website_category(driver, page=page)

    list_cate_id = cate_dict['list_cate_id']
    for cate in list_cate_id:
        cat_id = cate['cat_id']
        sub_cats = cate['sub_cats']
        cat_path = 'data/shopee/{}'.format(cat_id)
        for sub_cat in sub_cats:
            sub_cat_id = sub_cat['sub_cat_id']
            sub_cat_url = sub_cat['url']
            sub_cat_path = '{}/{}'.format(cat_path, sub_cat_id)

            try:
                products_df = pd.read_csv('data/shopee/{}/{}/list_products.csv'.format(cat_id, sub_cat_id))
                n_product += len(products_df)
                print(sub_cat_path, len(products_df))
            except:
                os.remove('data/shopee/{}/{}/list_products.csv'.format(cat_id, sub_cat_id))
                print('cat_id: {}   sub_cat_id: {}    url: {}'.format(cat_id, sub_cat_id, sub_cat_url))
                list_products = get_list_product(driver, url=sub_cat_url)
                page = 1
                sub_cat_url_page = sub_cat_url + '?page={}'
                while True:
                    list_product = get_list_product(driver, url=sub_cat_url_page.format(page))
                    page += 1
                    if len(list_product) == 0:
                        break
                    else:
                        list_products.extend(list_product)
                if len(list_products) > 0:
                    products_df = pd.DataFrame(list_products)
                    products_df.to_csv(sub_cat_path + '/list_products.csv', index=False)
                    n_product += len(products_df)
                    print(sub_cat_path, len(products_df))
    driver.close()
    print(n_product)


if __name__ == '__main__':
    main()

