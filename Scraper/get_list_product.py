from bs4 import BeautifulSoup
from selenium import webdriver
import time
import re
import json
import os
import pandas as pd
from Scraper.structure.home_page import HomePage


def get_website_category(driver, page: HomePage):
    data_path = 'data/{}'.format(page.name)
    if os.path.exists('data') is False:
        os.mkdir('data')
    if os.path.exists(data_path) is False:
        os.mkdir(data_path)
    driver.get(page.url)
    time.sleep(1)
    html = driver.page_source
    bs = BeautifulSoup(html, 'html.parser')
    categories = bs.findAll(class_=page.cat_tag)
    list_cate = []
    for cate in categories:
        cate_ = cate.find(class_='_2APAEl')
        cate_name = cate_.text
        cate_url = cate_.attrs['href']
        cat_id = re.findall('cat\.[\d]+$', cate_url)[0][4:].strip(' \n')
        sub_cates = cate.findAll(class_=page.sub1_cat_tag)
        list_sub_cate = []

        cate_path = 'data/{}/{}'.format(page.name, cat_id)
        if os.path.exists(cate_path) is False:
            os.mkdir(cate_path)

        for sub_cate in sub_cates:
            sub_cate_name = sub_cate.text
            sub_cate_url = sub_cate.attrs['href']
            sub_cat_id = re.findall('\.[\d]+$', sub_cate_url)[0][1:].strip(' \n')

            sub_cate_path = '{}/{}'.format(cate_path, sub_cat_id)
            if os.path.exists(sub_cate_path) is False:
                os.mkdir(sub_cate_path)

            list_sub_cate.append({
                'sub_cat_id': sub_cat_id,
                'name': sub_cate_name,
                'url': page.url + sub_cate_url
            })
        list_cate.append({
            'cat_id': cat_id,
            'name': cate_name,
            'url': page.url + cate_url,
            'sub_cats': list_sub_cate
        })

    cate_dict = {
        'name': page.name,
        'url': page.url,
        'list_cate_id': list_cate
    }
    with open('data/{}/category.json'.format(page.name), 'w', encoding='utf-8') as pf:
        json.dump(cate_dict, pf)

    return cate_dict


def get_list_product(driver, url):
    try:
        driver.get(url)
        print(url)
        time.sleep(1)
    except:
        print("Don't get response")

    html = driver.page_source
    bs = BeautifulSoup(html, 'html.parser')
    list_box = bs.findAll('script', {'type': 'application/ld+json',
                              'data-rh': 'true'})
    if len(list_box) < 3:
        return []

    list_box = list_box[3:]

    list_product = []
    for box in list_box:
        content = box.contents[0]
        content = json.loads(content)
        offers = content['offers']
        if 'price' in offers:
            price = offers['price']
        else:
            price = None

        if 'aggregateRating' in content:
            aggregateRating = content['aggregateRating']
        else:
            aggregateRating = {"bestRating": None, "worstRating": None, "ratingCount":None, "ratingValue":None}

        # print('\t\t productID: {}, name: {}'.format(content['productID'], content['name']))
        list_product.append(
            {
                'name': content['name'],
                'description': content['description'],
                'url': content['url'],
                'brand': content['brand'],
                'productID': content['productID'],
                'aggregateRating': aggregateRating,
                'price': price
            }
        )
    return list_product


